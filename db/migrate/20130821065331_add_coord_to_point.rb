class AddCoordToPoint < ActiveRecord::Migration
  def up
    add_column :points, :coord_x1, :integer
    add_column :points, :coord_y1, :integer
    add_column :points, :coord_x2, :integer
    add_column :points, :coord_y2, :integer
  end

  def down
    remove_column :points, :coord_x1
    remove_column :points, :coord_y1
    remove_column :points, :coord_x2
    remove_column :points, :coord_y2
  end

end
