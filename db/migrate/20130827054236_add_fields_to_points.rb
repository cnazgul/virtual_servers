class AddFieldsToPoints < ActiveRecord::Migration
  def up
    add_column :points, :type, :integer
    add_column :points, :ip, :string
    add_column :points, :os, :string
    add_column :points, :netname, :string
  end

  def down
    remove_column :points, :type
    remove_column :points, :ip
    remove_column :points, :os
    remove_column :points, :netname
  end
end
