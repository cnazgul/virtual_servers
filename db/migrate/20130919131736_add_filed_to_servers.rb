class AddFiledToServers < ActiveRecord::Migration
  def up
    add_column :servers, :reg_change, :integer, :default => 1
  end

  def down
    remove_column :servers, :reg_change
  end
end
