class AddFieldsToPoint < ActiveRecord::Migration
  def up
    add_column :points, :number_rack, :integer
    add_column :points, :server_id, :integer
    add_column :points, :position_in_rack, :string
  end

  def down
    remove_column :points, :number_rack
    remove_column :points, :server_id
    remove_column :points, :position_in_rack
  end

end
