class AddTable < ActiveRecord::Migration
  def up
    drop_table :roles_users
    create_table :roles_users, :id => false do |t|
      t.references :role, :user
    end
  end

  def down
    drop_table :roles_users
  end
end
