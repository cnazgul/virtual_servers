class ChangeFieldsToPoints < ActiveRecord::Migration
  def up
    remove_column :points, :type
    add_column :points, :type_srv, :integer
    add_column :points, :bond, :integer
  end

  def down
    add_column :points, :type, :integer
    remove_column :points, :type_srv
    remove_column :points, :bond
  end
end
