$(document).ready(function () {

// Выделение текста - полужирный и ссылки
    function getTextFieldSelection(textField) {
        return textField.value.substring(textField.selectionStart, textField.selectionEnd);
    };

    $('#point_description').select(function(){
        if (getTextFieldSelection(document.getElementsByTagName("textarea")[0]))
        {
            textComponent = document.getElementsByTagName("textarea")[0];
            s = getTextFieldSelection(document.getElementsByTagName("textarea")[0]);
            startPos = textComponent.selectionStart;
            endPos = textComponent.selectionEnd;

            $("#addlink").click(function () {
                newSel = '<a href="http://'+ s +'" target="_blank">' + s + '</a>';
                textComponent.value = textComponent.value.substr(0,startPos)+newSel+textComponent.value.substr(endPos);

                textComponent = null;
                s = null;
                startPos = null;
                endPos = null;

            });

            $("#addbold").click(function () {
                newSel = '<b>' + s + '</b>';
                textComponent.value = textComponent.value.substr(0,startPos)+newSel+textComponent.value.substr(endPos);

                textComponent = null;
                s = null;
                startPos = null;
                endPos = null;

            });

        }
    });

  flag_j = 0;

// Плагин для подсветки областей area
  $('img').mapster({
    fillOpacity: 0.4,
    fillColor: "d42e16",
    strokeColor: "3320FF",
    strokeOpacity: 0.8,
    strokeWidth: 2,
    stroke: true,
    isSelectable: false,
    singleSelect: false
  });

// Плагин для выделения областей на фото
    if (gon.user_access == 1) {
    $('#img_server').imgAreaSelect ({
        onSelectEnd: function (img, selection) {
            $('input#point_coord_x1').val(selection.x1);
            $('input#point_coord_y1').val(selection.y1);
            $('input#point_coord_x2').val(selection.x2);
            $('input#point_coord_y2').val(selection.y2);

            if (selection.x1 != selection.x2) {
// заполняем уникальный id для группы серверов на одной области изображения
              $('input#point_bond').val(Math.floor(Math.random() * 10000));
// готовим вид менюшки для добавления сервера к имеющимся
              $("select#point_number_rack, select#point_type_srv").show();
              $('input#point_ip').val("");
              $('input#point_os').val("");
              $('input#point_netname').val("");
              $('input#point_position_in_rack').val("");
              $('select#point_number_rack').val("0");
              $('select#point_type_srv').val("0");

              $("fieldset#add_menu").css("height","500px");
              $("input#point_position_in_rack").show();
              $("fieldset#add_menu").show();
// даем потаскать
              $("fieldset#add_menu").draggable({ containment:'parent'});

            } else {
              $("fieldset#add_menu").hide();
            }
        }
    });
    }

// форма регистрации
    $(".reg").click(function() {
        $("fieldset#reg_menu").toggle();
    });
    $("fieldset#reg_menu").mouseup(function() {
        return false
    });
    $(document).mouseup(function(e) {
        $("fieldset#reg_menu").hide();
    });

// форма входа
    $(".sign").click(function() {
        $("fieldset#signin_menu").toggle();
    });
    $("fieldset#signin_menu").mouseup(function() {
        return false
    });
    $(document).mouseup(function(e) {
        $("fieldset#signin_menu").hide();
    });


// Показать информацию о сервере (show) по клику на область фото
    $('area').click(function() {
      var i = $(this).attr("id");
      $.ajax({
        dataType: "script",
        url: '/?point=' + i,
        success: function(html) {
          $("fieldset#show_menu").show();
        }
      });

    });

// Модальное окно списка серверов
    $("#all_servers").click(function() {
        flag_i = 0;
        $("tr").css("background","#f3faff");
        if (flag_j == 0) {
          $("fieldset#all_srv").show();
          flag_j = 1;
        } else {
          $("fieldset#all_srv").hide();
          flag_j = 0;
        }
    });

    $(document.body).click(function(e) {
        var target = $(e.target);

        if (target.is('img') || target.is('#main') || target.is('.navbar-inner')) {
            $("fieldset#show_menu").hide();
            $("fieldset#all_srv").hide();
            flag_j = 0;
            $("fieldset#add_menu").hide();
        } else if (target.is('.drag') || target.is('.drag:p')) {
            $("fieldset#add_menu").hide();
        } else {

        }
    });

// Подстветка и общее поведение блока tr, при клике на определенный сервер в общем списке серверов
    $('.point').click(function(a) {
        var target = $(a.target);
        if (target.is('.del')){
            $(this).hide();
        }
        else {
        if (flag_i !== $(this).attr("id")) {
          flag_i = $(this).attr("id");
          $("tr").css("background","#f3faff");
          $('tr[id=' + flag_i + ']').css("background","#E4F1FF");

            $.ajax({
              dataType: "script",
              url: '/?point=' + flag_i,
              success: function(html) {
                 $("fieldset#show_menu").show();
                 $("fieldset#add_menu").hide();
              }
          });
        } else {
            $('tr[id=' + flag_i + ']').css("background","#f3faff");
            $("fieldset#show_menu").hide();
            $("fieldset#add_menu").hide();
            flag_i = 0;
        };
        };
    });

// Подсвечиваем области на фото, при наведении на строку с сервером в общем меню списка серверов (all_point)
    $('.point').mouseenter(function() {
        $('area#' + $(this).attr("id")).trigger('mouseover');
    });
    $('.point').mouseleave(function() {
        $('area').trigger('mouseout');
    });

// Поиск (точнее фильтрация)
    $("input#ttt").keyup(function(){
/*
// Подкрашивание найденного
       var searchTerm = $("input#ttt").val();
       $('tr').css('color','black');

        if (searchTerm.length > 1) {
          $('tr:contains('+ searchTerm +')').css('color','red');
        } else {
          $('tr:contains('+ searchTerm +')').css('color','black');
        }
*/
// Скрывать, что не подходит под заданный фильтр
        var searchTerm = $("input#ttt").val();
// регистронезависимый
        $.extend($.expr[":"], {
            "containsNC": function(elem, i, match, array) {
                return (elem.textContent || elem.innerText || "").toLowerCase
                    ().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });

        $('tr.point').show();
        if (searchTerm.length > 1) {
            $('tr.point').not('tr.point:containsNC('+ searchTerm +')').hide();
        } else {
            $('tr.point').show();
        }
    });


    $('.reg_change').change(function() {
        reg_change = $(this).val();
        $.ajax({
            dataType: "script",
            url: '/?reg_change=' + reg_change,
            success: function(html) {
              if (reg_change == 1)
                alert("Регистрация включена");
              else
                alert("Регистрация отключена");
            }
        });
    });

    $('.servers_room').change(function() {
        var url = '/?servers_room=' + $(this).val(); // get selected value
        if (url) { // require a URL
            window.location = url; // redirect
        }
    });

    $("#show_menu").draggable({ containment:'parent', handle: 'div.drag'});

    /*
    tinyMCE.init({
        mode: 'textareas',
        theme: 'advanced',
        theme_advanced_toolbar_location : "top",
        theme_advanced_buttons1 : "bold, link, unlink"
    });
    */
});
