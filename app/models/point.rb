class Point < ActiveRecord::Base
  extend PointModule
  attr_accessible :description, :name, :owner, :coord_x1, :coord_y1, :coord_x2, :coord_y2, :ip, :os, :netname, :type_srv, :bond, :number_rack, :server_id, :position_in_rack

  validates :name, :owner, :coord_y1, :coord_x2, :coord_x1, :coord_y2, :ip, :os, :type_srv, presence: true
  validates :name, :owner, :ip, :os, length: {in: 7..35}
  validates :description, length: {in: 20..200}
  validates :coord_y2, :coord_x1, :coord_x2, :coord_y1, numericality: true

  belongs_to :server

  scope :points_view, ->(room_id) { where("server_id = ?", room_id).order("name ASC") }
  scope :point_bond, ->(point) { where('bond = ?', point) }

  def self.unload_point(gon,servers_room_id)
    gon.x1 = []
    gon.y1 = []
    gon.x2 = []
    gon.y2 = []
    Point.where('server_id = ?', servers_room_id).each do |p|
      gon.x1[p.id] = p.coord_x1
      gon.y1[p.id] = p.coord_y1
      gon.x2[p.id] = p.coord_x2
      gon.y2[p.id] = p.coord_y2
    end
  end
end
