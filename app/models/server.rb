require 'file_size_validator'

class Server < ActiveRecord::Base
  attr_accessible :title, :image, :id

  has_many :points
  mount_uploader :image, ImageUploader

  validates :title, :image, presence: true
  validates :title, length: { in: 5..15}
  validates :image,
            :file_size => {
                :maximum => 3.megabytes.to_i
            }

end
