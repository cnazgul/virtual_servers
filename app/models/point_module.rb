module PointModule
  def user_access(gon,current_user)
    if current_user.present?
      gon.user_access = current_user.role_ids
    else
      gon.user_access = 0
    end
  end

  def reg_change(param)
    @reg_change = Server.first
    @reg_change.reg_change = param
    @reg_change.save
  end
end