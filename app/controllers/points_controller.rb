class PointsController < ApplicationController

  load_and_authorize_resource :except => [:index]
  before_filter :find_point, :only => [:destroy, :update, :edit]

  # GET /points
  # GET /points.json
  def index
    @point = Point.new
    params_if()
    @reg_change = Server.first.reg_change
    Point.unload_point(gon,@@servers_room_id)
    Point.user_access(gon,current_user)

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /points/1/edit
  def edit

  end

  # POST /points
  # POST /points.json
  def create
    @point = Point.new(params[:point])
    @point.server_id = @@servers_room_id

    respond_to do |format|
      if @point.save
        format.html { redirect_to root_path }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /points/1
  # PUT /points/1.json
  def update

    respond_to do |format|
      if @point.update_attributes(params[:point])
        format.html { redirect_to root_path }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /points/1
  # DELETE /points/1.json
  def destroy
    @point.destroy

    respond_to do |format|
      format.js
    end
  end

  private
  def find_point
    @point = Point.find(params[:id].to_i)
  end

  def params_if
    servers_room = params[:servers_room].to_i
    if servers_room.nonzero?
      @server = Server.find(servers_room)
      @points = Point.points_view(servers_room)
      @@servers_room_id = servers_room
    elsif params[:reg_change]
      Point.reg_change(params[:reg_change].to_i)
    elsif params[:point]
      @points = Point.point_bond(params[:point].to_i)
    else
      @server = Server.first
      @points = Point.points_view(1)
      @@servers_room_id = 1
    end
  end

end
