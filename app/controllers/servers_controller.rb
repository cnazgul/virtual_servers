class ServersController < ApplicationController

  load_and_authorize_resource
  before_filter :find_server, :except => [:index, :new, :create]

  # GET /servers
  # GET /servers.json
  def index
    # ограничим отображение количества серверных комнат (реальное кол-во серверных в организации 5)
    @servers = Server.limit(10)

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /servers/1
  # GET /servers/1.json
  def show

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /servers/new
  # GET /servers/new.json
  def new
    @server = Server.new

    respond_to do |format|
      format.html # new.html.slim
    end
  end

  # GET /servers/1/edit
  def edit

  end

  # POST /servers
  # POST /servers.json
  def create
    @server = Server.new(params[:server])

    respond_to do |format|
      if @server.save
        format.html { redirect_to servers_path}
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /servers/1
  # PUT /servers/1.json
  def update

    respond_to do |format|
      if @server.update_attributes(params[:server])
        format.html { redirect_to  servers_path}
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /servers/1
  # DELETE /servers/1.json
  def destroy
    @server.destroy

    respond_to do |format|
      format.html { redirect_to servers_url }
    end
  end

  private
  def find_server
    @server = Server.find(params[:id].to_i)
  end

end
