# coding: utf-8
module PointsHelper

  def edit_server(point)
    if (!current_user.nil? and current_user.role? :administrator)
      raw "<td><a href=\"points/#{point.id}/edit\">#{point.name}</a></td>"
    else
      raw "<th>#{point.name}</th>"
    end
  end

  def add_server
    if (!current_user.nil? and current_user.role? :administrator)
      raw "<p class=\"supple_srv\">&nbsp; Добавить сервер</p>"
    end
  end

  def server_type(type_srv)
   if (type_srv == 1)
     "Физический"
   elsif (type_srv == 2)
     "Виртуальный"
   else
     "NULL"
   end
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

end
